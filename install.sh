#! /usr/bin/env bash

set -e
set -x

BASEDIR=$(dirname $0)

function inflate_workspace {
    # inflates ./workspaces/$1.tgz to /data/lb_deployment/inflated/$1
    ws=$1
    rm -rf "${LB_DEPLOYMENT_HOME}/inflated/${ws}"
    mkdir -p "${LB_DEPLOYMENT_HOME}/inflated/${ws}"
    tar xfz "./workspaces/${ws}.tgz" -C "${LB_DEPLOYMENT_HOME}/inflated/${ws}"
}

function import_workspace {
    # imports /data/lb_deployment/inflated/$1 to workspace named "/${1}"
    ws=$1
    lb import-workspace --overwrite "/${ws}" "${LB_DEPLOYMENT_HOME}/inflated/${ws}"
}

for workspace in demand-forecast-sample
do
    inflate_workspace "${workspace}"
    import_workspace "${workspace}"
done

exit 0
