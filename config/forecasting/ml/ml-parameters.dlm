PARAMETER|VALUE|COMMENTS
ORDER|2|all
SGD_ITER|20|all
CG_ITER|5|all
RANK|5|all
REG_TYPE|2|all
REG_PARAM|1.0e-4,1.0e-4|all
REG_MODEL||all
REG_SUPPORT_POW|0|all
REG_SUPPORT_MULT|1|all
ADAPTIVE_SGD|true|all, highly recommended to set it to true
ETA_TYPE|0|all, not relevant with adaptive_sgd
ETA|1.0|all, not relevant with adaptive_sgd
ETA_ALPHA|1.0|all, not relevant with adaptive_sgd
ETA_BETA|1.0|all, not relevant with adaptive_sgd
ITER_NUM|0|all, not relevant with adaptive_sgd
QUANTILE|-1.0|all
QUANTILE_EPS|1.0|all
EXCLUDE_MIN|0|all
EXCLUDE_PCT|0.0|all
HAS_BIAS|true|all
NUDGED||all
INIT_SAMPLE_NUM|0|all
INIT_SAMPLE_PCT|0.0|all
NUM_THREADS|1|all
MIN_FORECAST|0.01|all, configurable only in EDB mode right now
MAX_FORECAST||all, configurable only in EDB mode right now

CLUSTERS|1|lfama
MIN_CLUSTER_SIZE|0|lfama
KMEANS_ITER|10|lfama
KMEANS_ON||lfama
LOCAL_ONLY|false|lfama, associated with warm start

SALES_RANGES|0.0|sfama
TELESCOPING|true|sfama
CLASSIFIER_MAX_SGD|15|sfama 
CLASSIFIER_MAX_CG|5|sfama 
APPROXIMATE_EXPECTATION|false|sfama
CLASSIFIER_ORDER|2|sfama
CLASSIFIER_OBJECTIVE|2|sfama

SPLITON|product_prod|dfama
MAXDEPTH|2|dfama
PARTITION_MAP|3:4|dfama

NODES_PER_LAYER|2|mlp

