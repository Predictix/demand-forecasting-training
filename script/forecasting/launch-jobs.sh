#!/usr/bin/env bash

set -e

partitions_to_run_file=${1}
s3_folder=${2}
tag=${3}
user_name=${4}
user_key=${5}
  
main(){

  IFS=","

  # Create mapping file between partition and lb-steve job id
  echo "partition|job_id" > ${tag}.log

  cat ${partitions_to_run_file} | while read partition ws_name settings_file
  do
    if [ ${partition} != "PARTITION" ]
    then  
      echo " --- Partition : ${partition} - workspace: ${ws_name} - settings file: ${settings_file}"  
      echo "partition|${partition}" >> log_${tag}.tmp 

      lb-steve create-job --impl dft --timeout 40000 -i ${s3_folder}/data/${partition}/ -i ${s3_folder}/data/infor/ -i ${s3_folder}/warmup-coeffs/${partition}/coeffs/ -i ${s3_folder}/lb-steve/deploy/upstream/ -i ${s3_folder}/lb-steve/deploy/out/ -o ${s3_folder}/output/${tag}/${partition} -m platform='4.3.14' -m job-queue=r3-8xlarge -m LB_SUPPRESS_DOMAIN_PARALLELISM=1 -m DFT_PARTITION=${partition} -m DFT_SETTINGS_FILE=${settings_file} -m DFT_WORKSPACE_NAME=${ws_name} -u ${user_name} -k ${user_key} > log_${tag}.tmp

      job_id=$(cut -d\" -f4 log_${tag}.tmp)
      echo "${partition}|${job_id}" >> ${tag}.log
    fi
  done 
}

main
