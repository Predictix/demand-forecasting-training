#! /usr/bin/env bash

set -e
set -x

PARTITION=${1}
WS_NAME=${2}
RUN_SETTINGS_FILE=${3}

#--- Read all relevant parameter settings 
get-param()
{
  value=$(grep -w ${1} ${2} | cut -d\| -f2 )
  echo ${value}
}

SALES_FILE=$(get-param "SALES_INPUT_FILE" ${RUN_SETTINGS_FILE})

#--- Set up run folder structure
EXECUTION_HOME=${PWD}
ELIGIBILITY_HOME=${EXECUTION_HOME}/export/${PARTITION}
mkdir -p ${ELIGIBILITY_HOME}

function training_run()
{
  bash ${DEMAND_MGMT_HOME}/script/forecasting/eligibility.sh \
    train ${1} ${2} \
    config/forecasting/eligibility-settings.dlm \
    ${ELIGIBILITY_HOME} >> ${ELIGIBILITY_HOME}/eligibillity_run.log
}

function evaluation_run()
{
  bash ${DEMAND_MGMT_HOME}/script/forecasting/eligibility.sh \
    eval ${1} ${2} \
    config/forecasting/eligibility-settings.dlm \
    ${ELIGIBILITY_HOME} >> ${ELIGIBILITY_HOME}/eligibillity_run.log
}

function main()
{
  #--- create calendar file
  lb exec ${WS_NAME} '_(d) <- cal:day(d).' --print --exclude-ids --csv 
  echo "DAY_ID" > ${ELIGIBILITY_HOME}/calendar.dlm
  sed 's/"//g' _.csv | cut -d " " -f 1 | sort -u >> ${ELIGIBILITY_HOME}/calendar.dlm
  rm _.csv

  #--- get time frame from sales file
  tail -n+2 ${EXECUTION_HOME}/${SALES_FILE} | cut -d "|" -f 4 | sort -u > ${ELIGIBILITY_HOME}/eligibillity_dates.dat
  start_date=$(head -1 ${ELIGIBILITY_HOME}/eligibillity_dates.dat)
  end_date=$(tail -1 ${ELIGIBILITY_HOME}/eligibillity_dates.dat)
 
  #--- Training on the horizon ${start_date} to ${end_date}
  training_run ${start_date} ${end_date}

  #--- Evaluating eligibilities ...
  evaluation_run ${start_date} ${end_date}

  #--- Importing eligibility
  time lb web-client import http://localhost:8080/${WS_NAME}/eligibility \
    -i file://${ELIGIBILITY_HOME}/evaluated_eligibilities.dlm -p -n -T 28800
}

main
