#! /usr/bin/env bash

set -e

export RC_SUCCESS=0
export RC_FAILURE=1

sUsage='
Usage: create-partition-settings-file.sh <partition> <input master-config file> <output settings-file >
 This script accepts 3 arguments: 
 - the partition name
 - the input master config file (containing attributes/features per partition)
 - the output file with the attributes/features corresponding for the given partition  
 The master config file is expected to have a list of partitions 
  as well as a default one that will be used for any partition that
  that is not listed in the file.
'

#--- Check the number of arguments to the script
if [ "$#" -lt "3" ]
then
  echo
  echo "ERROR: Wrong number of arguments
  ${sUsage}" >&2
  exit ${RC_FAILURE}
fi

#--- Reset the arguments
partition=""
master_config=""
output_file=""

partition=${1}
master_config=${2}
output_file=${3}

#--- Check for input file
if [[ ! -f ${master_config} ]]; then
  echo "ERROR: $master_config does not exist. Stopping..."
  echo
  echo "${sUsage}" >&2
  exit ${RC_FAILURE}
fi

#--- Check for multiple entries of the same partition & use "default" if none found
if [[ $(grep -w $partition ${master_config} | wc -l) -gt 1 ]]; then
  echo "ERROR: Multiples lines for ${partition} in ${master_config}. Exiting..."
  exit ${RC_FAILURE}
else 
  if [[ $(grep -w ${partition} ${master_config} | wc -l) -ne 1 ]]; then
    echo "Partition is not found in ${master_config}. default settings will be used."
    partition="default"
  fi
  
  partition_values=$(grep -w ${partition} ${master_config} | cut -d\| -f2)
  
  # If the parameters are still empty (which likely means the 'default' line is not found), then exit
  echo "Attributes from ${master_config} are ${partition_values}"
  values_count=$(echo ${partition_values} | awk -F"," '{print NF}')
fi

#--- Loop through atrributes build the output file
echo "attributes/features" > ${output_file}
i=1
while [[ ${i} -le ${values_count} ]]; do 
  value=$(echo ${partition_values} | cut -d"," -f $i)

  if [[ ! ${value} == "" ]]; then
    echo ${value} >> ${output_file}
  fi
  
  i=$((i+1)); 
done

exit ${RC_SUCCESS}
