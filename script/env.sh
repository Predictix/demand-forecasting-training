# get script location
if [[ -n $BASH_VERSION ]]; then
  _SOURCED_SCRIPT="${BASH_SOURCE[0]}"
elif [[ -n $ZSH_VERSION ]]; then
  _SOURCED_SCRIPT="${(%):-%N}"
else
  echo "Shell not supported"
  return
fi

export APP_HOME=$(cd $(dirname $_SOURCED_SCRIPT)/.. ; pwd)
export APP_UPSTREAM=${MYUPSTREAM:=$APP_HOME/upstream}

# Set up Scala
export SCALAC_HOME=${APP_UPSTREAM}/scala/bin
export PATH=${SCALAC_HOME}:${PATH}

# setup LB environment
export LB_COMPONENTS=${APP_UPSTREAM}/logicblox
source $LB_COMPONENTS/etc/profile.d/logicblox.sh
export LB_CONNECTBLOX_ENABLE_ADMIN=1

# dependent projects
export RETAIL_MODEL_HOME=${APP_UPSTREAM}/retail-model/out
export DEMAND_MGMT_HOME=${APP_UPSTREAM}/demand-mgmt/out

# Variables needed for Model Refinery
export MR_COMMON_LIB_PROJECT=custom
export MR_DEPENDENCIES=custom_dims,dft_feature_sampling
export MR_LINK_FEATURES=dft_feature_sampling:features
